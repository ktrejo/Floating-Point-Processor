`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:34:13 05/08/2017 
// Design Name: 
// Module Name:    Ascii_to_Hex 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Ascii_to_Hex(
    input [7:0] Ascii,
    output [3:0] Hex
    );

	assign Hex = (Ascii == 8'b00110000) ? 4'b0000 :
					 (Ascii == 8'b00110001) ? 4'b0001 :
					 (Ascii == 8'b00110010) ? 4'b0010 :
					 (Ascii == 8'b00110011) ? 4'b0011 :
					 (Ascii == 8'b00110100) ? 4'b0100 :
					 (Ascii == 8'b00110101) ? 4'b0101 :
					 (Ascii == 8'b00110110) ? 4'b0110 :
					 (Ascii == 8'b00110111) ? 4'b0111 :
					 (Ascii == 8'b00111000) ? 4'b1000 :
					 (Ascii == 8'b00111001) ? 4'b1001 :
					 (Ascii == 8'b01100001) ? 4'b1010 :
					 (Ascii == 8'b01100010) ? 4'b1011 :
					 (Ascii == 8'b01100011) ? 4'b1100 :
					 (Ascii == 8'b01100100) ? 4'b1101 :
					 (Ascii == 8'b01100101) ? 4'b1110 :
					 (Ascii == 8'b01100110) ? 4'b1111 : 4'b1111;

endmodule
