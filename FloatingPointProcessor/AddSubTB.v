`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   15:10:07 05/03/2017
// Design Name:   FloatingPointAdderSubtractor
// Module Name:   H:/project1/AddSubTB.v
// Project Name:  project1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: FloatingPointAdderSubtractor
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module AddSubTB;

	// Inputs
	reg [31:0] a;
	reg [31:0] b;
	reg sub;
	reg clk;
	reg rst;

	// Outputs
	wire [31:0] result;
	wire done;

	// Instantiate the Unit Under Test (UUT)
	FloatingPointAdderSubtractor uut (
		.a(a), 
		.b(b), 
		.sub(sub), 
		.clk(clk), 
		.rst(rst), 
		.result(result), 
		.done(done)
	);
	
	integer i = 0;

	initial begin
		// Initialize Inputs
		a = 0;
		b = 0;
		sub = 0;
		clk = 0;
		rst = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
				
		a = 32'h4023d70a;
		b = 32'h436a0000;
		rst = 1;
		clk = 1;
		#5;
		rst = 0;
		clk = 0;
		#5
		for (i = 0; i < 80; i = i + 1) begin
			clk = ~clk;
			#5;
		end		
		a = 32'h42c7fae1;
		b = 32'h3e24dd2f;
		sub = 1;
		rst = 1;
		clk = 1;
		#5;
		rst = 0;
		clk = 0;
		#5
		for (i = 0; i < 80; i = i + 1) begin
			clk = ~clk;
			#5;
		end
		a = 32'h00001a00;
		b = 32'h0000012e;
		sub = 1;
		rst = 1;
		clk = 1;
		#5;
		rst = 0;
		clk = 0;
		#5
		for (i = 0; i < 80; i = i + 1) begin
			clk = ~clk;
			#5;
		end
	end
      
endmodule

