`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:31:05 04/26/2017 
// Design Name: 
// Module Name:    FloatingPointProcessor 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module FloatingPointProcessor(
    input [65:0] input_stream,
	 input from_mult,
	 input rst,
	 input clk,
	 output reg to_mult,
	 output reg done,
    output reg [31:0] result_stream
    );

	reg [5:0] counter;
	reg [2:0] state;
	reg addreset;
	wire [31:0] addout;
	wire adddone;
	wire [5:0] counterMinusOne;
	
		
	FloatingPointAdderSubtractor addsub(input_stream[63:32], input_stream[31:0], input_stream[64], clk, addreset, addout, adddone);
	RippleAdder #(6) rip(.a(counter), .b(6'b000001), .sub(1'b1), .s(counterMinusOne), .c_out());
	
	always @ (posedge clk) begin
		if (rst) begin
			state <= 0;
			addreset <= 0;
			counter = 63;
			to_mult = 0;
		end
		else begin
			case (state)
			0: begin
				if (input_stream[65] == 0) begin
					addreset <= 1;
					state <= 1;
				end
				else begin
					to_mult = 1;
					state <= 2;
				end
			end
			1: begin
				addreset <= 0;
				if (adddone == 1) begin
					result_stream = addout;
					done <= 1;
					state <= 5;
				end
			end
			2: begin
				if (counter > 0) begin
					to_mult = input_stream[counter];
					counter = counterMinusOne;
				end
				else begin
					to_mult = input_stream[counter];
					counter = 31;
					state <= 3;
				end
			end
			3: begin
				to_mult = 0;
				if (from_mult == 1) begin 
					state <= 4;
				end
			end
			4: begin
				if (counter > 0) begin
					result_stream[counter] = from_mult;
					counter = counterMinusOne;
				end
				else begin
					result_stream[counter] = from_mult;
					done <= 1;
					state <= 5;
				end
			end
			5: begin
				result_stream = 0;
				done <= 0;
			end
			endcase
		end
	end

endmodule
