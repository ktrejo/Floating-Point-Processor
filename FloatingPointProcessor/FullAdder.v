`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:03:53 02/22/2017 
// Design Name: 
// Module Name:    FullAdder 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module FullAdder(
    input x,
    input y,
    input z,
    output s,
    output c
    );
	 
	 wire pairs [2:0];

	xor x1(s, x, y, z);
	and a1(pairs[0], x, y);
	and a2(pairs[1], x, z);
	and a3(pairs[2], y, z);
	or o1(c, pairs[0], pairs[1], pairs[2]);

endmodule
