`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:44:55 04/26/2017 
// Design Name: 
// Module Name:    FloatingPointAdderSubtractor 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module FloatingPointAdderSubtractor(
    input [31:0] a,
    input [31:0] b,
    input sub,
	 input clk,
	 input rst,
    output [31:0] result,
	 output reg done
    );
	 
	reg g;
	reg r;
	reg s;
	reg [1:0] shifts;
	reg [24:0] mantout;
	wire [24:0] sum;
	reg [31:0] a1;
	reg [31:0] b1;
	reg [7:0] expa;
	reg [7:0] expb;
	reg [23:0] manta;
	reg [23:0] mantb;
	reg [2:0] stall;
	reg [2:0] state;
	reg [7:0] expToShift;
	reg minus;
	wire operation = a[31] ^ b[31] ^ sub;
	wire [2:0] stallPlusOne;
	wire [23:0] negativeMantB;
	wire [7:0] expShift;
	wire [24:0] mantoutPlusOne;
	
	RippleAdder #(24) adder(.a(manta), .b(mantb), .sub(1'b0), .s(sum[23:0]), .c_out(sum[24]));
	RippleAdder #(3) rip(.a(stall), .b(3'b001), .sub(1'b0), .s(stallPlusOne), .c_out());
	RippleAdder #(24) mantbflip(.a(24'b000000000000000000000000), .b(mantb), .sub(1'b1), .s(negativeMantB), .c_out());
	RippleAdder #(8) addexp(.a(expToShift), .b(8'b00000001), .sub(minus), .s(expShift), .c_out());
	RippleAdder #(25) incrementMantout(.a(mantout), .b(25'b0000000000000000000000001), .sub(1'b0), .s(mantoutPlusOne), .c_out());
	
	assign result = {a1[31], expa, mantout[22:0]};
	
	always @ (posedge clk) begin
		if (rst) begin
			a1 <= a;
			b1 <= b;
			state <= 0;
			done <= 0;
			shifts <= 0;
			g <= 0;
			r <= 0;
			s <= 0;
		end
		else begin
			case (state)
			0: begin
				if ((a1[30:23] == 63 && a1[22:0] != 0) || (b1[30:23] == 63 && b1[22:0] != 0)) begin
					expa <= 8'b11111111;
					mantout <= 25'b1111111111111111111111111;
					done <= 1;
					state <= 7;
				end
				else if (a1[30:23] == 63) begin
					if (b1[30:23] == 63 && (a1[31] ^ b1[31] ^ sub == 1)) begin
						expa <= 8'b11111111;
						mantout <= 25'b1111111111111111111111111;
						done <= 1;
						state <= 7;
					end
					else begin
						expa <= 8'b11111111;
						mantout <= 25'b0000000000000000000000000;
						done <= 1;
						state <= 7;
					end
				end
				else if (b1[30:23] == 63) begin
					a1[31] <= b1[31];
					expa <= 8'b11111111;
					mantout <= 25'b0000000000000000000000000;
					done <= 1;
					state <= 7;
				end
				else if (a1[30:23] < b1[30:23]) begin
					a1 <= b1;
					b1 <= a1;
				end
				else if (a1[22:0] < b1[22:0]) begin
					a1 <= b1;
					b1 <= a1;
				end
				state <= 1;
			end
			1: begin
				expa <= a1[30:23];
				expb <= b1[30:23];
				if (a1[30:23] == 0)
					manta <= {1'b0, a1[22:0]};
				else
					manta <= {1'b1, a1[22:0]};
				if (b1[30:23] == 0)
					mantb <= {1'b0, b1[22:0]};
				else
					mantb <= {1'b1, b1[22:0]};
				state <= 2;
			end
			2: begin
				if (operation) begin
					mantb <= negativeMantB;
				end
				expToShift <= expb;
				state <= 3;
				minus <= 0;
			end
			3: begin
				if (expa > expb) begin
					if (shifts == 0) begin
						shifts <= 1;
						g <= mantb[0];
					end
					else if (shifts == 1) begin
						shifts <= 2;
						r <= mantb[0];
					end
					else begin
						s <= s | mantb[0];
					end
					if (operation) begin
						mantb <= {1'b1, mantb[23:1]};
					end
					else begin
						mantb <= mantb >> 1;
					end
					expb <= expShift;
					expToShift <= expShift;
				end
				else begin
					shifts <= 0;
					stall <= 0;
					state <= 4;
				end
			end
			4: begin
				if (stall < 7) begin
					stall <= stallPlusOne;
				end
				else begin
					minus <= (~mantout[24]) | operation;
					expToShift <= expa;
					mantout <= sum;
					state <= 5;
				end
			end
			5: begin
				if (mantout[24] == 1 && operation == 0) begin
					r <= mantout[0];
					s <= g | r | s;
					mantout <= mantout >> 1;
					expa <= expShift;
					expToShift <= expShift;
					state <= 6;
				end
				else if (mantout[23] == 0) begin
					if (expa == 0) begin
						state <= 6;
					end
					else begin
						if (shifts == 0) begin
							mantout <= {mantout, g};
							shifts <= 1;
						end
						else begin
							mantout <= mantout << 1;
							r <= 0;
							s <= 0;
						end
						expa <= expShift;
						expToShift <= expShift;
					end
				end
				else begin
					if (shifts == 0) begin
						r <= g;
						s <= r | s;
					end
					state <= 6;
					expToShift <= expa;
					minus <= 0;
				end
			end
			6: begin
				if ((r & mantout[0]) | (r & s)) begin
					mantout <= mantoutPlusOne;
					r <= 0;
				end
				else if (mantout[24] == 1 && operation == 0) begin
					mantout <= mantout >> 1;
					expa <= expShift;
				end
				else begin
					if (expa == 63) begin
						mantout <= 25'b0000000000000000000000000;
					end
					done <= 1;
					state <= 7;
				end
			end
			7: begin
				done <= 0;
				a1 <= 0;
				b1 <= 0;
				expa <= 0;
				expb <= 0;
				manta <= 0;
				mantb <= 0;
				shifts <= 0;
				mantout <= 0;
			end
			endcase
		end
	end

endmodule
