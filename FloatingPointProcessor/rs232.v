`timescale 1ns / 1ps

// New Version
// Modifided by Khai Lai (4/18/2012)

module rs232(
  input from_mult,
  output to_mult,
  input               CLK_50M,
  output shared_clk,
  input       [ 1: 0] SW,
  output              UART_TX,
  input               UART_RX,
  input               resetIn
);


// Clock Resets Signals
  wire          clock_50M;
  wire          clock_200M;
  wire          dcm_locked;
  wire          arst_n = dcm_locked;

// Shared clock
	assign shared_clk = clock_50M;
	
// UART Signals
  wire        [15: 0] divisor;
  wire        [ 7: 0] rx_data;
  wire                rx_irq;
  reg         [ 7: 0] tx_data;
  reg                 tx_wr;
  wire                tx_irq;
  wire                tx_tcvr;

/*************************************************************/
//             CLOCK GEN AND COUNTERS                        //
/*************************************************************/

   reg        RST_IN;
   reg  [3:0] RST_Counter;
   wire       CLKIN_IBUFG_OUT;
  

	clock_gen01 MYclock_gen01 (
		 .CLKIN_IN(CLK_50M), 
		 .RST_IN(RST_IN), 
		 .CLKFX_OUT(clock_200M), 
		 .CLKIN_IBUFG_OUT(CLKIN_IBUFG_OUT), 
		 .CLK0_OUT(clock_50M), 
		 .LOCKED_OUT(dcm_locked)
    );
 
	parameter [1:0] RESET  = 2'b00,
	                WAIT   = 2'b01,
						 LOCKED = 2'b10;
	reg [1:0] nx_state;

	always @ (posedge CLKIN_IBUFG_OUT or posedge resetIn) begin
		if (resetIn)	nx_state <= RESET;
	   else begin           
			case (nx_state)
				RESET     : begin
									RST_Counter <= 0;
									RST_IN <= 1;
									nx_state <= WAIT;
								end
				
				WAIT      : begin
									RST_Counter <= RST_Counter + 1;
									RST_IN <= 1;
									if (RST_Counter > 3)
										 nx_state <= LOCKED;
									else
										 nx_state <= WAIT;
							  end
				
				LOCKED    : begin
									RST_IN <= 0;
									nx_state <= LOCKED;
								end
				
				default   : begin
									RST_Counter <= 0;
									RST_IN <= 1;							   
								end						 
			endcase
		end
	end




/*************************************************************/
//                  RS-232 TRANCEIVER                        //
/*************************************************************/
parameter clk_freq = 50000000;
parameter baud = 115200;
assign UART_TX = (SW[0]) ? UART_RX : tx_tcvr;
assign divisor = clk_freq/baud/16;

uart_transceiver transceiver(
	.sys_clk          ( clock_50M ),
	.sys_rst          ( ~arst_n   ),
	.uart_rx          ( UART_RX   ),
	.uart_tx          ( tx_tcvr   ),
	.divisor          ( divisor   ),
	.rx_data          ( rx_data   ),
	.rx_done          ( rx_irq    ),
	.tx_data          ( tx_data   ),
	.tx_wr            ( tx_wr     ),
	.tx_done          ( tx_irq    )
);



/*************************************************************/
//                  PLACE YOUR CODE HERE                     //
/*************************************************************/

	// Need
	reg [3:0] counter = 0;
	reg [65:0] stream = 0;
	reg readyToSend = 0;
	reg [31:0] result;
	reg [1:0] state;
	wire [3:0] hex_data;
	wire controllerDone;
	wire [3:0] ascii_to_hex;
	wire [7:0] hex_to_ascii;
	wire [31:0] controllerResult;
	wire [3:0] counterPlusOne;
	
	Ascii_to_Hex ath (rx_data, ascii_to_hex);
	Hex_to_Ascii hta (hex_data, hex_to_ascii);
	// Need Controller
	FloatingPointProcessor flop(stream, from_mult, readyToSend, clock_50M, to_mult, controllerDone, controllerResult);
	RippleAdder #(4) rip(.a(counter), .b(4'b0001), .sub(1'b0), .s(counterPlusOne), .c_out());
	
	//assign hex_data = stream[63:60];
	assign hex_data = result[31:28];

	always @ (posedge clock_50M) begin
	
		if (resetIn) begin
			counter <= 0;
			stream <= 8;
			state <= 0;
			readyToSend <= 0;
			tx_wr <= 0;
			result <= 0;
		end
		else case (state)
		0: begin
			if (rx_irq) begin
				if (counter < 1) begin
					// ascii code of 1
					if (rx_data == 8'b00110001)
						stream <= {stream, 1'b1};
					else
						stream <= {stream, 1'b0};
					counter <= counterPlusOne;
				end
				else begin
					// ascii code of 1
					if (rx_data == 8'b00110001)
						stream <= {stream, 1'b1};
					else
						stream <= {stream, 1'b0};
					counter <= 0;
					state <= 1;
				end
			end
		end
		1: begin
			if (rx_irq) begin
				if (counter < 15) begin
					stream <= {stream, ascii_to_hex};
					counter <= counterPlusOne;
				end
				else begin
					stream <= {stream, ascii_to_hex};
					counter <= 0;
					state <= 2;
					readyToSend <= 1;
				end
			end
		end
		2: begin
			readyToSend <= 0;
			if (controllerDone) begin
				state <= 3;
				result <= controllerResult;
			end
		end
		3: begin
			tx_wr <= 0;
			if ((tx_irq == 1 && counter < 8) || counter == 0) begin
				tx_wr <= 1;
				tx_data <= hex_to_ascii;
				result <= result << 4;
				counter <= counterPlusOne;
			end
			else if (counter >= 8) begin
				counter <= 0;
				result <= 0;
				state <= 0;
				stream <= 0;
			end
		end
		endcase
	end

endmodule 
