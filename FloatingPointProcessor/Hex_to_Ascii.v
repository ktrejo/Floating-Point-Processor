`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:19:07 05/08/2017 
// Design Name: 
// Module Name:    Hex_to_Ascii 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Hex_to_Ascii(
    input [3:0] Hex,
    output [7:0] Ascii
    );
	 
	 // If error occurs ascii prints !
	 assign Ascii = (Hex == 4'b0000) ? 8'b00110000  :
						 (Hex == 4'b0001) ? 8'b00110001  :
						 (Hex == 4'b0010) ? 8'b00110010  :
						 (Hex == 4'b0011) ? 8'b00110011  :
						 (Hex == 4'b0100) ? 8'b00110100  :
						 (Hex == 4'b0101) ? 8'b00110101  :
						 (Hex == 4'b0110) ? 8'b00110110  :
						 (Hex == 4'b0111) ? 8'b00110111  :
						 (Hex == 4'b1000) ? 8'b00111000  :
						 (Hex == 4'b1001) ? 8'b00111001  :
						 (Hex == 4'b1010) ? 8'b01100001  :
						 (Hex == 4'b1011) ? 8'b01100010  :
						 (Hex == 4'b1100) ? 8'b01100011  :
						 (Hex == 4'b1101) ? 8'b01100100  :
						 (Hex == 4'b1110) ? 8'b01100101  :
						 (Hex == 4'b1111) ? 8'b01100110  : 8'b00100001;


endmodule
