`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:58:05 04/26/2017 
// Design Name: 
// Module Name:    RippleAdder 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module RippleAdder #(parameter size=24) (
    input [size-1:0] a,
    input [size-1:0] b,
    input sub,
    output [size-1:0] s,
    output c_out
    );

	wire [size-1:0] realb;
	wire [size:0] carry;
	assign carry[0] = sub;
	assign c_out = carry[size];
	
	generate
		genvar j;
		for (j = 0; j < size; j = j + 1)
			begin: A
				xor xj(realb[j], b[j], sub);
				FullAdder faj(a[j], realb[j], carry[j], s[j], carry[j+1]);
			end
	endgenerate

endmodule
