`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   17:55:35 05/09/2017
// Design Name:   FloatingPointProcessor
// Module Name:   H:/project1/FloatProcessorTB.v
// Project Name:  project1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: FloatingPointProcessor
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module FloatProcessorTB;

	// Inputs
	reg [65:0] input_stream;
	reg from_mult;
	reg rst;
	reg clk;

	// Outputs
	wire to_mult;
	wire done;
	wire [31:0] result_stream;

	// Instantiate the Unit Under Test (UUT)
	FloatingPointProcessor uut (
		.input_stream(input_stream), 
		.from_mult(from_mult), 
		.rst(rst), 
		.clk(clk), 
		.to_mult(to_mult), 
		.done(done), 
		.result_stream(result_stream)
	);
	integer i;

	initial begin
		// Initialize Inputs
		input_stream = 0;
		from_mult = 0;
		rst = 0;
		clk = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		
		rst = 1;
		#1;
		clk = 1;
		#4;
		rst = 0;
		clk = 0;
		input_stream = 66'b000100000000100011110101110000101001000011011010100000000000000000;
		#5;
		for (i = 0; i < 200; i = i + 1) begin
			clk = ~clk;
			#5;
		end

	end
      
endmodule

