`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:16:49 05/08/2017 
// Design Name: 
// Module Name:    mantissa_product 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module mantissa_product(
	input [22:0] a,
	input [22:0] b,
	input norm_a,
	input norm_b,
	output [47:0] p
    );

	wire [29:0] pp [8:0];
	
	pp_generator ppg(
		.m(a),
		.a(b),
		.norm_m(norm_a),
		.norm_a(norm_b),
		.pp_0(pp[0]),
		.pp_1(pp[1][28:0]),
		.pp_2(pp[2][28:0]),
		.pp_3(pp[3][28:0]),
		.pp_4(pp[4][28:0]),
		.pp_5(pp[5][28:0]),
		.pp_6(pp[6][28:0]),
		.pp_7(pp[7][26:0]),
		.pp_8(pp[8][23:0])
	);
	
	wallace_tree tree_add(
		.pp_0(pp[0]),
		.pp_1(pp[1][28:0]),
		.pp_2(pp[2][28:0]),
		.pp_3(pp[3][28:0]),
		.pp_4(pp[4][28:0]),
		.pp_5(pp[5][28:0]),
		.pp_6(pp[6][28:0]),
		.pp_7(pp[7][26:0]),
		.pp_8(pp[8][23:0]),
		.sum(p)
	);
endmodule
