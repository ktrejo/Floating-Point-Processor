`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:35:00 02/22/2017 
// Design Name: 
// Module Name:    CSA16 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module CSA #(parameter size=16)(
    input [size-1:0] x,
    input [size-1:0] y,
    input [size-1:0] z,
    output [size-1:0] s,
    output [size-1:0] c
    );

	generate
		genvar j;
		for (j = 0; j < size; j = j + 1) 
			begin: A
				FullAdder faj(x[j], y[j], z[j], s[j], c[j]);
			end
	endgenerate
		
endmodule
