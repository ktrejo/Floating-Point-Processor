`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   18:24:04 05/15/2017
// Design Name:   Controller
// Module Name:   H:/FloatingPointMultiplier/ControllerTB.v
// Project Name:  FloatingPointMultiplier
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: Controller
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module ControllerTB;

	// Inputs
	reg from_add;
	reg clk;
	reg rst;

	// Outputs
	wire to_add;

	// Instantiate the Unit Under Test (UUT)
	Controller uut (
		.from_add(from_add), 
		.clk(clk), 
		.rst(rst), 
		.to_add(to_add)
	);
	integer i;

	initial begin
		// Initialize Inputs
		from_add = 0;
		clk = 0;
		rst = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		
		rst = 1;
		#1;
		clk = 1;
		#5;
		rst = 0;
		clk = 0;
		#5;
		
		for (i = 0; i < 400; i = i + 1) begin
			from_add = ~from_add;
			#1
			clk = ~clk;
			#4;
			clk = ~clk;
			#5;
		end

	end
      
endmodule

