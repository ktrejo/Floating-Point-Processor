`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:44:44 03/27/2017 
// Design Name: 
// Module Name:    bcd_counter_4bit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module bcd_counter_4bit(clk, en, count, reset);
    input clk, en;
    output [3:0] count;
    input reset;
	 
	 reg [3:0] count;
	 
	 initial begin
		count = 4'b0;  
	 end 
	 
	 always @(posedge clk) 
	 begin
		
		if (reset == 1'b1)
			count <= 4'b0;
		else if (en == 1'b1) begin
			if (count == 4'b1111)
				count <= 4'b0;
			else
				count <= count + 1'b1;
		end 
	 end

endmodule
