`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    20:02:26 05/08/2017 
// Design Name: 
// Module Name:    Controller 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Controller(
    input from_add,
	 input clk,
	 input rst,
    output reg to_add
    );
	 
	 reg [63:0] inputs;
	 reg [6:0] counter;
	 reg [2:0] state;
	 wire [31:0] p;
	 reg mult_reset;
	 wire [6:0] counterMinusOne;
	 
	 FloatingPointMultiplier mult(inputs[63:32], inputs[31:0], clk, mult_reset, p, multdone);
	 RippleAdder #(7) rip(.a(counter), .b(7'b0000001), .sub(1), .s(counterMinusOne), .c_out());
	 
	always @ (posedge clk or posedge rst) begin
		if (rst) begin
			state <= 0;
			inputs = 0;
			to_add = 0;
		end
		else case (state)
		0: begin
			to_add = 0;
			if (from_add == 1) begin
				state <= 1;
				counter = 63;
			end
		end
		1: begin
			if (counter > 0) begin
				inputs[counter] = from_add;
				counter = counterMinusOne;
			end
			else begin
				inputs[counter] = from_add;
				counter = 31;
				mult_reset <= 1;
				state <= 2;
			end 
		end
		2: begin
			mult_reset <= 0;
			if (multdone == 1) begin
				state <= 3;
				to_add = 1;
			end
		end
		3: begin
			if (counter > 0) begin
				to_add = p[counter];  
				counter = counterMinusOne;
			end
			else begin
				to_add = p[counter];
				state <= 0;
			end
		end
		endcase
	end

endmodule
