`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:43:24 05/08/2017 
// Design Name: 
// Module Name:    mantisa_fix 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module mantissa_round(
	input [24:0] old_mantissa_p,
	input [22:0] old_mantissa_a,
	input [8:0] exp,
	output reg [8:0] new_exp,
	output reg [22:0] fraction
   );
	
	wire [8:0] expPlusOne;
	wire [22:0] mantPlusOne;
	
	RippleAdder #(9) rip(.a(exp), .b(9'b000000001), .sub(1'b0), .s(expPlusOne), .c_out());
	
	always @(*) begin
		if (old_mantissa_p[24]) begin
			//s <= | saved_mant_excess;
			//r <= saved_partial_mant[0];
			if ((old_mantissa_p[1] & old_mantissa_p[0]) | (old_mantissa_p[0] & | old_mantissa_a))
				fraction <= old_mantissa_p[23:1] + 1'b1;
			else 
				fraction <= old_mantissa_p[23:1];
			new_exp <= expPlusOne;
		end else begin
			//s <= | saved_mant_excess[21:0];
			//r <= saved_mant_excess[22];
			if ((old_mantissa_p[0] & old_mantissa_a[22]) | (old_mantissa_a[22] & | old_mantissa_a[21:0]))
				fraction <= old_mantissa_p[22:0] + 1'b1;
			else 
				fraction <= old_mantissa_p[22:0];
			new_exp <= exp;
		end
	 end

endmodule
