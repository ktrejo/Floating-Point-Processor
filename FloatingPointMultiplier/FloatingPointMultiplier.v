`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:45:56 04/26/2017 
// Design Name: 
// Module Name:    FloatingPointMultiplier 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module FloatingPointMultiplier(
    input [31:0] a,
    input [31:0] b,
	 input clk,
	 input rst,
    output [31:0] p,
	 output reg done
    );
	 
	wire ctr_done;
	wire clk_div;
	clk_counter counter (.clk(clk), .rst(rst), .done(ctr_done));
	ClkDivider clock_divider (.clk(clk), .rst(rst), .clk_div(clk_div));	
	
	reg [31:0] a_in, b_in;
	wire [1:0] corner_case;
	corner_case ec (
		.a(a[30:0]),
		.b(b[30:0]),
		.corner_case(corner_case)
	);
	
	multiplier_pipeline mp (
		.a(a_in),
		.b(b_in),
		.clk(clk_div),
		.rst(rst),
		.corner_case(corner_case),
		.p(p)
	);
	
	// Corner case check
	always @(*) begin
		case (corner_case) 
			2'b00: begin
				a_in <= a;
				b_in <= b;
			end
			2'b01, 2'b10, 2'b11: begin
				a_in <= {a[31], 31'b0};
				b_in <= {b[31], 31'b0};
			end
		endcase
	end
	
	localparam 	RST = 2'b00,
					WAIT = 2'b01,
					DONE = 2'b10;
	reg [1:0] currState, nextState;
	
	// State selector
	always @(posedge clk or posedge rst) begin
		if (rst) begin
			currState <= RST;
		end
		else currState <= nextState;
	end
	
	// State transition
	always @(*) begin
		case (currState)
			RST: nextState <= WAIT;
			WAIT: begin
				if (ctr_done) nextState <= DONE;
				else nextState <= currState;
			end
			DONE: begin
				if (rst) nextState <= RST;
				else nextState <= currState;
			end
			default: nextState <= currState;
		endcase
	end
	
	// state logic
	always @(*) begin
		case (currState)
			DONE: begin
				done <= 1'b1;
			end
			default begin
				done <= 1'b0;
			end
		endcase
	end
	
endmodule
