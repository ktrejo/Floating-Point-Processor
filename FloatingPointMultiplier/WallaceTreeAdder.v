`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:35:40 05/03/2017 
// Design Name: 
// Module Name:    wallace_tree 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
	module wallace_tree(
	 input [29:0] pp_0,
	 input [28:0] pp_1, pp_2, pp_3, pp_4, pp_5, pp_6,
	 input [26:0] pp_7,
	 input [23:0] pp_8,
    output [47:0] sum
    );
	 
	// CSA00
	wire [28:0] out_0 [1:0]; 		// Outputs S00 and C00
	CSA #(29) csa_0(
		.x({2'b0, pp_0[29:3]}),
		.y(pp_1),
		.z({pp_2[25:0], 3'b0}),
		.s(out_0[0]),
		.c(out_0[1])
	);
	assign sum[2:0] = pp_0[2:0];
	assign sum[3] = out_0[0][0];
	
	// CSA01
	wire [28:0] out_1 [1:0]; 		// Outputs S01 and C01
	CSA #(29) csa_1(
		.x({3'b0, pp_3[28:3]}),
		.y(pp_4),
		.z({pp_5[25:0], 3'b0}),
		.s(out_1[0]),
		.c(out_1[1])
	);
	
	// CSA02
	wire [26:0] out_2 [1:0]; 		// Outputs S02 and C02
	CSA #(27) csa_2(
		.x({1'b0, pp_6[28:3]}),
		.y(pp_7),
		.z({pp_8, 3'b0}),
		.s(out_2[0]),
		.c(out_2[1])
	);
	
	
	// CSA03
	wire [30:0] out_3 [1:0]; 		// Outputs S03 and C03
	CSA #(31) csa_3(
		.x({pp_2[28:26], out_0[0][28:1]}),
		.y({2'b0, out_0[1]}),
		.z({out_1[0][22:0], pp_3[2:0], 5'b0}),
		.s(out_3[0]),
		.c(out_3[1])
	);
	assign sum[4] = out_3[0][0];
	
	// CSA04
	wire [29:0] out_4 [1:0]; 		// Outputs S04 and C04
	CSA #(30) csa_4(
		.x({6'b0, out_1[1][28:5]}),
		.y({out_2[0], pp_6[2:0]}),
		.z({out_2[1][25:0], 4'b0}),
		.s(out_4[0]),
		.c(out_4[1])
	);
	
	// CSA05
	wire [38:0] out_5 [1:0]; 		// Outputs S05 and C05
	CSA #(39) csa_5(
		.x({pp_5[28:26], out_1[0][28:23], out_3[0][30:1]}),
		.y({8'b0, out_3[1]}),
		.z({out_4[0][25:0], out_1[1][4:0], 8'b0}),
		.s(out_5[0]),
		.c(out_5[1])
	);
	assign sum[5] = out_5[0][0];
	
	// CSA06
	wire [41:0] out_6 [1:0]; 		// Outputs S06 and C06
	CSA #(42) csa_6(
		.x({out_4[1][28:0], 13'b0}),
		.y({out_4[0][29:26], out_5[0][38:1]}),
	 	.z({3'b0, out_5[1]}),
		.s(out_6[0]),
		.c(out_6[1])
	); 
	assign sum[6] = out_6[0][0];

 
	// CPA 
	//assign sum[47:7] = {out_3[1][27], out_5[0][39:1]} + out_5[1];
	RippleAdder #(41) cpa(
		.a(out_6[0][41:1]),
		.b(out_6[1][40:0]),
		.sub(1'b0), 
		.s(sum[47:7])
		//.c_out(sum[47])
	); 
	
endmodule
