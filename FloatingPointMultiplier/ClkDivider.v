`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:48:57 05/10/2017 
// Design Name: 
// Module Name:    ClkDivider 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ClkDivider(
	 input clk,
	 input rst,
	 output reg clk_div
    );
	 
	reg count;
	
	always @ (posedge clk) begin
		if (rst) begin
			count <= 1'b0;
			clk_div <= 1'b0;
		end
		else begin
			count <= ~count;
		end
		if (count == 1) begin
			clk_div <= ~clk_div;
		end
	end
	
endmodule
