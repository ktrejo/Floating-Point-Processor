`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:44:44 03/27/2017 
// Design Name: 
// Module Name:    bcd_counter_4bit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module clk_counter (
    input clk, rst,
	 output reg done
	 );
	 
	localparam constNum = 4;
	
	wire [2:0] countPlusOne;
	reg [2:0] count;
	
	RippleAdder #(3) rip(.a(count), .b(4'b0001), .sub(1'b0), .s(countPlusOne), .c_out());
	
	always @ (posedge clk) begin
		if (rst) begin
			count <= 0;
			done <= 1'b0;
		end
		else if (count != constNum) begin
			count <= countPlusOne;
		end
		else begin
			done <= 1'b1;
		end
	end

endmodule
