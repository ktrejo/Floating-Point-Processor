`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    20:36:07 05/08/2017 
// Design Name: 
// Module Name:    corner_case_check 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module corner_case(
	 input [30:0] a, b,
	 output reg [1:0] corner_case
    );
	
	always @(*) begin
		// a or b is NaN => NaN
		if ((& a[30:23] && | a[22:0]) || (& b[30:23] && | b[22:0])) begin
			corner_case = 2'b10;
		end else  
		// a = 0 and b = inf => NaN
		if ( ~| a && (& b[30:23] && ~| b[22:0])) begin
			corner_case = 2'b10;
		end else 
		// b = 0 and a = inf => NaN
		if ((& a[30:23] && ~| a[22:0]) && ~| b) begin
			corner_case = 2'b10;
		end else 
		// a or b is infinity => Inf
		if ((& a[30:23] && ~| a[22:0]) || (& b[30:23] && ~| b[22:0])) begin
			corner_case = 2'b01;
		end else  
		// a or b 0 and other non-zero, non-infinity and non-Nan => 0
		if (~| a || ~| b) begin
			corner_case = 2'b11;
		end else begin
			corner_case = 2'b00;
		end
	end

endmodule
