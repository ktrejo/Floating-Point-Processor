`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   16:03:06 05/15/2017
// Design Name:   Controller
// Module Name:   E:/Enee359F/FloatingPointMultiplier/mult_controller_tb.v
// Project Name:  FloatingPointMultiplier
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: Controller
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module mult_controller_tb;

	// Inputs
	reg from_add;
	reg clk;

	// Outputs
	wire to_add;
	wire [63:0] inputs;
	wire [31:0] p;
	wire multdone;
	wire [2:0] state;

	// Instantiate the Unit Under Test (UUT)
	Controller uut (
		.from_add(from_add), 
		.clk(clk), 
		.p(p),
		.inputs(inputs),
		.multdone(multdone),
		.state(state),
		.to_add(to_add) 
	); 

	integer i;
	reg [63:0] data_in = 64'h4159999ac0e9999a;
	reg data_in_done;
	initial begin
		// Initialize Inputs
		data_in_done = 0;
		from_add = 0;
		clk = 0;
		#2; from_add = 1;
		#2;
		for (i = 0; i <64; i = i + 1) 
		begin
			from_add = data_in[63 - i]; 
			#2;
		end
		data_in_done = 1;
		
		
		from_add = 0; #5;
		from_add = 1;
		
		#60;
		$finish;
		
		
	end
	
	always begin
		clk = ~clk;
		#1;
	end
      
endmodule

