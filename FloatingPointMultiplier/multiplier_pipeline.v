`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:00:41 05/09/2017 
// Design Name: 
// Module Name:    verified_input_multiplier 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module multiplier_pipeline(
	 input [31:0] a,
    input [31:0] b,
	 input clk,
	 input rst,
	 input [1:0] corner_case,
    output [31:0] p
    );

/*************************************************************/
//     		Pipeline step 0: Save inputs					       //
/*************************************************************/	 
//	reg s_in;
//	reg [1:0] corner_case_in;
//	reg [31:0] a_in, b_in;
//	always @(posedge clk) begin
//		if (rst) begin
//			a_in <= 0;
//			b_in <= 0;
//			s_in <= 0;
//			corner_case_in <= 0;
//		end else begin
//			a_in <= a;
//			b_in <= b;
//			s_in <= a[31] ^ b[31];
//			corner_case_in <= corner_case;
//		end
//	end


/*************************************************************/
//     		Pipeline step I: Exp bias, Mant product	       //
/*************************************************************/	
	wire initial_sign;
	wire [1:0] initial_corner_case;
	wire[8:0]	initial_exp;
	reg 			saved_initial_sign;
	reg [1:0] 	saved_initial_corner_case;
	reg [8:0] 	saved_initial_exp;
	
	// Exponent sum and bias
	exponent_bias exp_bias (
		.exp_a(a[30:23]),
		.exp_b(b[30:23]),
		.old_corner_case(corner_case),
		.new_corner_case(initial_corner_case),
		.biased_exp(initial_exp)
	);
	
	// Sign bit
	xor(initial_sign, a[31], b[31]);
	
	wire [24:0] initial_mant; 
	wire [22:0] initial_mant_error;
	reg [22:0] 	saved_initial_mant_error;
	reg [24:0] 	saved_initial_mant; 	// 24 bits to include guard bit
	
	// Mantisa product
	mantissa_product mp(
		.a(a[22:0]),
		.b(b[22:0]),
		.norm_a(| a[30:23]),
		.norm_b(| b[30:23]),
		.p({initial_mant, initial_mant_error})
	);
	
	always @(posedge clk or posedge rst) begin
		if (rst) begin
			saved_initial_mant <= 0;
			saved_initial_mant_error <= 0;
			saved_initial_exp <= 0;
			saved_initial_sign <= 0;
			saved_initial_corner_case <= 0;
		end else begin
			saved_initial_mant <= initial_mant;
			saved_initial_mant_error <= initial_mant_error;
			saved_initial_exp <= initial_exp;
			saved_initial_sign <= initial_sign;
			saved_initial_corner_case <= initial_corner_case;
		end
	end

/*************************************************************/
//     		Pipeline step II: Rounding Mantisa Product       //
/*************************************************************/	
	wire 			rounding_sign;
	wire [1:0]  rounding_corner_case;
	wire [8:0]	rounded_exp;
	wire [22:0] rounding_fraction;

	reg 			saved_rounding_sign;
	reg [1:0] 	saved_rounding_corner_case;
	reg [8:0] 	saved_rounded_exp;
	reg [22:0] 	saved_rounding_fraction;

	assign rounding_sign = saved_initial_sign;
	assign rounding_corner_case = saved_initial_corner_case;
	mantissa_round mr (
		.old_mantissa_p(saved_initial_mant),
		.old_mantissa_a(saved_initial_mant_error),
		.exp(saved_initial_exp),
		.new_exp(rounded_exp),
		.fraction(rounding_fraction)
	);
	always @(posedge clk or posedge rst) begin
		if (rst) begin
			saved_rounded_exp <= 0;
			saved_rounding_fraction <= 0;
			saved_rounding_sign <= 0;
			saved_rounding_corner_case <= 0;
		end else begin
			saved_rounded_exp <= rounded_exp;
			saved_rounding_fraction <= rounding_fraction;
			saved_rounding_sign <= rounding_sign;
			saved_rounding_corner_case <= rounding_corner_case;
		end
	end

/*************************************************************/
//     		Pipeline done, pick exponent and output	       //
/*************************************************************/
	wire bias_sign;
	wire [1:0] bias_corner_case;
	wire [7:0] biased_exp;
	wire [22:0] bias_fraction;
	
	assign bias_sign = saved_rounding_sign;
	assign bias_fraction = saved_rounding_fraction;
	assign bias_corner_case = saved_rounding_corner_case;
	assign biased_exp = saved_rounded_exp;
	 
/*************************************************************/
//     		Pipeline step IV: Error Checking (ec)	          //
/*************************************************************/
	wire sign;
	wire [1:0] corner;
	wire [7:0] exp;
	wire [22:0] fraction;
	
	assign sign = bias_sign;
	assign corner = bias_corner_case;
	assign exp = biased_exp;
	assign fraction = bias_fraction;
	
/*************************************************************/
//     		 Final Output 		  							          //
/*************************************************************/
	reg saved_sign;
	reg [22:0] saved_fraction;
	reg [7:0] saved_exp;
	
	always @(*) begin
		if (rst) begin
			saved_fraction <= 0;
			saved_exp <= 0;
			saved_sign <= 0;
		end else begin
			case (corner)
				2'b00: begin
					saved_exp <= exp;
					saved_fraction <= fraction;
				end
				2'b01: begin // Inf
					saved_exp <= 8'd255;
					saved_fraction <= 23'b0;
				end
				2'b10: begin // NaN
					saved_exp <= 8'd255;
					saved_fraction <= 23'h7FFFFF;
				end
				2'b11: begin	// 0
					saved_exp <= 8'd0;
					saved_fraction <= 23'd0;
				end
			endcase
			
			saved_sign <= sign;
		end
	end

	assign p[31] = saved_sign;
	assign p[30:23] = saved_exp;
	assign p[22:0] = saved_fraction;
endmodule
