`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:23:06 05/08/2017 
// Design Name: 
// Module Name:    exponent_sum 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module exponent_sum(
	input [7:0] a,
	input [7:0] b,
	output [8:0] s
    );

	RippleAdder #(8) add_exp(
		.a(a),
		.b(b),
		.sub(1'b0),
		.s(s[7:0]),
		.c_out(s[8])
	);
	
endmodule
