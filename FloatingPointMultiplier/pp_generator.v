`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:34:31 05/03/2017 
// Design Name: 
// Module Name:    pp_generator 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module pp_generator(
    input [22:0] m, a,
	 input norm_m, norm_a, // hidden bit
    output [29:0] pp_0,
	 output [28:0] pp_1, pp_2, pp_3, pp_4, pp_5, pp_6,
	 output [26:0] pp_7,
	 output [23:0] pp_8
	 );
	
	reg [24:0] data;
	reg [29:0] pp_inout [7:0];

	assign pp_0 = pp_inout[0];
	assign pp_1 = pp_inout[1];
	assign pp_2 = pp_inout[2];
	assign pp_3 = pp_inout[3];
	assign pp_4 = pp_inout[4];
	assign pp_5 = pp_inout[5];
	assign pp_6 = pp_inout[6];
	assign pp_7 = pp_inout[7];
	
	wire [24:0] a1, a2;
	wire [25:0] a3, a4;
	assign a1 = {norm_a, a};
	assign a2 = a1 << 1;
	RippleAdder #(25) adder (.a(a1), .b(a2), .sub(1'b0), .s(a3[24:0]), .c_out(a3[25]));
	assign a4 = a1 << 2;
	
	reg [25:0] temp;
	reg S;
	integer i;
	
	always @(*)
	begin
		data = {norm_m, m, 1'b0}; // add the hidden bit
		//a1 = {3'b001, a}; 	// Sign extend 1 bit to account for multiplication by 4 and added hidden bit
		//a2 = a1 << 1;
		//a3 = a2 + a1;
		//a4 = a1 << 2;
		S = 0;
		 
		for (i = 0; i < 8; i = i + 1) begin
			case(data[3:0])
				4'b0000, 4'b1111: begin 
					temp = 25'b0;  			// +-0
					S = 0;
				end
				4'b0001, 4'b0010: begin
					temp = a1;					// +1x
					S = 0;
				end
				4'b0011, 4'b0100: begin
					temp = a2;					// +2x
					S = 0;
				end
				4'b0101, 4'b0110: begin 
					temp = a3;					// +3x
					S = 0;
				end
				4'b0111:	begin
					temp = a4;					// +4x
					S = 0;
				end
				4'b1000:	begin
					temp = ~a4 + 1'b1;		// -4x
					S = 1;
				end
				4'b1001, 4'b1010: begin
					temp = ~a3 + 1'b1;		// -3x
					S = 1;
				end
				4'b1011, 4'b1100: begin
					temp = ~a2 + 1'b1;		// -2x
					S = 1;
				end
				4'b1101, 4'b1110: begin 
					temp = ~a1 + 1'b1;		// -1x
					S = 1;
				end
			endcase 
			 
			// Deal with reduced sign extension
			
			if (i == 0) begin
				pp_inout[i] = {~S, S, S, S, temp};
			end else if (i == 7) begin
				pp_inout[i] = {~S, temp};
			end else begin
				pp_inout[i] = {2'b11, ~S, temp};
			end
			
			data = data >> 3;
		end
	end 
	
	assign pp_8 = a1;
	
endmodule
