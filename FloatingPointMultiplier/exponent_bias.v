`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:00:15 05/08/2017 
// Design Name: 
// Module Name:    exponent_bias 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module exponent_bias(
	input [7:0] exp_a,
	input [7:0] exp_b,
	input [1:0] old_corner_case,
	output reg [1:0] new_corner_case,
	output [8:0] biased_exp
    );
	 
	wire [8:0] temp_sum;
	wire temp_overflow, temp_underflow;
	RippleAdder #(8) exp_sum (
		.a(exp_a),
		.b(exp_b),
		.sub(1'b0),
		.s(temp_sum[7:0]),
		.c_out(temp_sum[8])
	);
	  
	RippleAdder #(10) exp_bias (
		.a({1'b0, temp_sum}),
		.b(10'd127),
		.sub(1'b1),
		.s({temp_underflow, temp_overflow, biased_exp[7:0]}),
		.c_out(biased_exp[8])
	);
	
	always @(*) begin
		if ( ~| old_corner_case) begin
			if (temp_underflow) 
				new_corner_case <= 2'b11; // 0
			else 
			if (temp_overflow)
				new_corner_case <= 2'b01; // Inf
			else
				new_corner_case <= 2'b00; // Normal
		end 
		else new_corner_case <= old_corner_case;
	end
endmodule
